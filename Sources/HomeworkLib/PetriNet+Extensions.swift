import PetriNet

extension PetriNet {

  /// Returns whether the given transition is fireable from the given marking.
  public func isFireable(transition: TransitionType, from marking: Marking<PlaceType>) -> Bool {

    /// Get the precondition corresponding to the given transition
    /// return whether all the places in the given pre conditions 
    /// require less tokens than their respective marking contain
    if let pre = input[transition] {
      return pre.keys.allSatisfy({ marking[$0] >= pre[$0]! })
    }

    /// return true by default represents 
    /// the case where the transition has 
    /// no preconditions
    return true
  }

  /// Returns the set of transitions that are fireable from the given marking.
  public func getFireableTransitions(from marking: Marking<PlaceType>) -> Set<TransitionType> {
    /// Retrieve all transitions
    let transitions = Set(input.keys).union(output.keys)
    /// filter the complete set of transitions using the previously defined function on each transition in set
    return transitions.filter { isFireable(transition: $0, from: marking) }
  }

}
