import XCTest

import HomeworkLibTests

var tests = [XCTestCaseEntry]()
tests += HomeworkLibTests.allTests()
XCTMain(tests)
